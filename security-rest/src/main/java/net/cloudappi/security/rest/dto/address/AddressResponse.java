package net.cloudappi.security.rest.dto.address;

import static net.cloudappi.security.rest.utils.AddressRestHelper.JsonProperties.ADDRESS_ID;
import static net.cloudappi.security.rest.utils.AddressRestHelper.JsonProperties.CITY;
import static net.cloudappi.security.rest.utils.AddressRestHelper.JsonProperties.COUNTRY;
import static net.cloudappi.security.rest.utils.AddressRestHelper.JsonProperties.STATE;
import static net.cloudappi.security.rest.utils.AddressRestHelper.JsonProperties.STREET;
import static net.cloudappi.security.rest.utils.AddressRestHelper.JsonProperties.ZIP;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import lombok.ToString;

/**
 * Se registra todos los usuarios<br>
 * <strong>t_addres</strong>
 */
@ToString
public class AddressResponse {

	/**
	 * ID AUTOGENERADO<br>
	 * <strong>addr_iident bigint NOT NULL</strong>
	 */
	@NotNull
	@Min(1)
	@JsonProperty(value = ADDRESS_ID, index = 1, required = true)
	@ApiModelProperty(position = 1, value = "ID AUTOGENERADO", required = true)
	private Long addressId;

	/**
	 * Nombre de la calle<br>
	 * <strong>addr_vstreet character varying</strong>
	 */
	@Size(min = 1, max = 100)
	@JsonProperty(value = STREET, index = 3)
	@ApiModelProperty(position = 3, value = "Nombre de la calle")
	private String street;

	/**
	 * Nombre del estado<br>
	 * <strong>addr_vstate character varying</strong>
	 */
	@Size(min = 1, max = 100)
	@JsonProperty(value = STATE, index = 4)
	@ApiModelProperty(position = 4, value = "Nombre del estado")
	private String state;

	/**
	 * Nombre de la ciudad<br>
	 * <strong>addr_vcity character varying</strong>
	 */
	@Size(min = 1, max = 100)
	@JsonProperty(value = CITY, index = 5)
	@ApiModelProperty(position = 5, value = "Nombre de la ciudad")
	private String city;

	/**
	 * Nombre del país<br>
	 * <strong>addr_vcountry character varying</strong>
	 */
	@Size(min = 1, max = 100)
	@JsonProperty(value = COUNTRY, index = 6)
	@ApiModelProperty(position = 6, value = "Nombre del país")
	private String country;

	/**
	 * Código Postal<br>
	 * <strong>addr_vzip character varying</strong>
	 */
	@Size(min = 1, max = 100)
	@JsonProperty(value = ZIP, index = 7)
	@ApiModelProperty(position = 7, value = "Código Postal")
	private String zip;

	/**
	 * ID AUTOGENERADO<br>
	 * <strong>addr_iident bigint NOT NULL</strong>
	 */
	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}

	/**
	 * ID AUTOGENERADO<br>
	 * <strong>addr_iident bigint NOT NULL</strong>
	 */
	public Long getAddressId() {
		return addressId;
	}

	/**
	 * Nombre de la calle<br>
	 * <strong>addr_vstreet character varying</strong>
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * Nombre de la calle<br>
	 * <strong>addr_vstreet character varying</strong>
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * Nombre del estado<br>
	 * <strong>addr_vstate character varying</strong>
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Nombre del estado<br>
	 * <strong>addr_vstate character varying</strong>
	 */
	public String getState() {
		return state;
	}

	/**
	 * Nombre de la ciudad<br>
	 * <strong>addr_vcity character varying</strong>
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Nombre de la ciudad<br>
	 * <strong>addr_vcity character varying</strong>
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Nombre del país<br>
	 * <strong>addr_vcountry character varying</strong>
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Nombre del país<br>
	 * <strong>addr_vcountry character varying</strong>
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Código Postal<br>
	 * <strong>addr_vzip character varying</strong>
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * Código Postal<br>
	 * <strong>addr_vzip character varying</strong>
	 */
	public String getZip() {
		return zip;
	}
}