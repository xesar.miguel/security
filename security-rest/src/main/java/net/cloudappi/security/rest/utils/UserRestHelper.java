package net.cloudappi.security.rest.utils;

public class UserRestHelper {

	public static class JsonProperties {
		public static final String USER_ID = "id";
		public static final String NAME = "name";
		public static final String EMAIL = "email";
		public static final String BIRTHDATE = "birthdate";
		public static final String ADDRESS = "address";
	}
}