package net.cloudappi.security.rest.controller;

import static com.escalableapps.framework.rest.model.HttpConstants.MediaType.APPLICATION_JSON;

import java.net.URI;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.escalableapps.framework.core.model.exception.NotFoundException;
import com.escalableapps.framework.core.service.ConversionService;
import com.escalableapps.framework.core.validation.group.Create;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import net.cloudappi.security.model.entity.Address;
import net.cloudappi.security.model.entity.User;
import net.cloudappi.security.model.service.OrquestadorService;
import net.cloudappi.security.model.vo.AddressUpdate;
import net.cloudappi.security.model.vo.UserAddress;
import net.cloudappi.security.model.vo.UserAddressUpdate;
import net.cloudappi.security.model.vo.UserUpdate;
import net.cloudappi.security.rest.dto.address.AddressResponse;
import net.cloudappi.security.rest.dto.user.UserRequest;
import net.cloudappi.security.rest.dto.user.UserResponse;
import net.cloudappi.security.rest.utils.HelperDate;

@RestController
@RequestMapping("/api/v1/")
@Validated
@Api
@Slf4j
public class SecurityRestController {

	private final OrquestadorService orquestadorService;
	private final ConversionService conversionService;

	public SecurityRestController(OrquestadorService orquestadorService, ConversionService conversionService) {
		this.orquestadorService = orquestadorService;
		this.conversionService = conversionService;
	}

	@GetMapping(path = "/users", produces = APPLICATION_JSON)
	@ApiOperation("Get all users")
	public ResponseEntity<List<UserResponse>> findUsers() {
		List<UserAddress> users = orquestadorService.findUsers();
		List<UserResponse> userPageResponses = conversionService.convert(users, UserResponse.class);
		log.debug("pageResponse={}", userPageResponses);
		return ResponseEntity.ok(userPageResponses);
	}

	@GetMapping(path = "/user/{userId}", produces = APPLICATION_JSON)
	@ApiOperation("Encontrar la entidad User según su identificador")
	public ResponseEntity<UserResponse> findUser( //
			@PathVariable(name = "userId", required = true) @ApiParam(value = "ID", required = true) @NotNull @Min(value = 1, message = "Invalid user id") Long userId //
	) {
		log.debug("userId={}", userId);
	
		UserAddress user = orquestadorService.findUser(userId);
		if (user == null) {
			return new ResponseEntity<UserResponse>(HttpStatus.NOT_FOUND);
		}
	
		UserResponse userResponse = conversionService.convert(user, UserResponse.class);
		log.debug("userResponse={}", userResponse);
		return ResponseEntity.ok(userResponse);
	}

	@PostMapping(path = "/user", consumes = APPLICATION_JSON, produces = APPLICATION_JSON)
	@Validated(Create.class)
	@ApiOperation("Crear una entidad User")
	public ResponseEntity<UserResponse> createUser( //
			@RequestBody(required = true) @ApiParam(value = "Se registra todos los usuarios", required = true) @NotNull @Valid UserRequest user //
	) {
		log.debug("userRequest={}", user);

		UserAddress userNew = convertRequest(user, UserAddress.class);

		userNew = orquestadorService.createUser(userNew.getUser(), userNew.getAddress());

		UserResponse userCreated = convertResponse(userNew, UserResponse.class);
		log.debug("userCreated={}", userCreated);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(userCreated.getUserId()).toUri();
		return ResponseEntity.created(location).body(userCreated);
	}

	@DeleteMapping(path = "/user/{userId}", produces = APPLICATION_JSON)
	@ApiOperation("Elimina una entidad User")
	public void deleteUser( //
			@PathVariable(name = "userId", required = true) @ApiParam(value = "ID", required = true) @NotNull @Min(1) Long userId //
	) {
		log.debug("userId={}", userId);
		orquestadorService.deleteUser(userId);
	}

	@PutMapping(path = "/user/{userId}", consumes = APPLICATION_JSON, produces = APPLICATION_JSON)
	@ApiOperation("Actualizar completamente una entidad User")
	public ResponseEntity<UserResponse> fullUpdateUser( //
			@PathVariable(name = "userId", required = true) @ApiParam(value = "id", required = true) @NotNull @Min(1) Long userId, //
			@RequestBody(required = true) @ApiParam(value = "Se registra todos los usuarios", required = true) @NotNull @Valid UserRequest user //
	) {
		
		log.debug("userId={}", userId);
		log.debug("userRequest={}", user);
		
		user.setFullUpdate(true);
		
		if (!orquestadorService.existsUser(userId)) {
			return new ResponseEntity<UserResponse>(HttpStatus.NOT_FOUND);
		}

		UserAddressUpdate userAddressUpdate = convertUpdate(user, UserAddressUpdate.class);

		if (userAddressUpdate != null) {
			userAddressUpdate.getUser().setUserId(userId);
			userAddressUpdate.getAddress().setUserId(userId);		
		}

		long updateCount = orquestadorService.updateUser(userAddressUpdate.getUser(), userAddressUpdate.getAddress());

		UserAddress userUpdate = orquestadorService.findUser(userId);
		UserResponse userResponse = conversionService.convert(userUpdate, UserResponse.class);
		log.debug("userResponse={}", userResponse);
		return ResponseEntity.ok(userResponse);
		
	}
	
	private UserAddressUpdate convertUpdate(UserRequest source, Class<UserAddressUpdate> type) {
		
		boolean fullUpdate = source.isFullUpdate();
		
		UserAddressUpdate target = new UserAddressUpdate();

		UserUpdate targetUser = new UserUpdate();
		
		if (fullUpdate || source.getName() != null || source.isNameNull()) {
			targetUser.setName(source.getName());
		}
		if (fullUpdate || source.getEmail() != null || source.isEmailNull()) {
			targetUser.setEmail(source.getEmail());
		}
		if (fullUpdate || source.getBirthdate() != null || source.isBirthdateNull()) {
			targetUser.setBirthdate(HelperDate.stringToDate(source.getBirthdate()).toInstant()
				      .atZone(ZoneOffset.of("Z")).toOffsetDateTime());
		}
		
		AddressUpdate targetAddress = new AddressUpdate();

		if (fullUpdate || source.getAddress().getStreet() != null || source.getAddress().isStreetNull()) {
			targetAddress.setStreet(source.getAddress().getStreet());
		}
		if (fullUpdate || source.getAddress().getState() != null || source.getAddress().isStateNull()) {
			targetAddress.setState(source.getAddress().getState());
		}
		if (fullUpdate || source.getAddress().getCity() != null || source.getAddress().isCityNull()) {
			targetAddress.setCity(source.getAddress().getCity());
		}
		if (fullUpdate || source.getAddress().getCountry() != null || source.getAddress().isCountryNull()) {
			targetAddress.setCountry(source.getAddress().getCountry());
		}
		if (fullUpdate || source.getAddress().getZip() != null || source.getAddress().isZipNull()) {
			targetAddress.setZip(source.getAddress().getZip());
		}
						
		target.setUser(targetUser);
		target.setAddress(targetAddress);
		
		return target;
	}

	private UserAddress convertRequest(UserRequest source, Class<UserAddress> type) {

		UserAddress target = new UserAddress();

		User targetUser = new User();
		targetUser.setName(source.getName());
		targetUser.setEmail(source.getEmail());
		targetUser.setBirthdate(HelperDate.stringToDate(source.getBirthdate()).toInstant().atZone(ZoneOffset.of("Z"))
				.toOffsetDateTime());

		Address targetAddress = new Address();
		targetAddress.setStreet(source.getAddress().getStreet());
		targetAddress.setState(source.getAddress().getState());
		targetAddress.setCity(source.getAddress().getCity());
		targetAddress.setCountry(source.getAddress().getCountry());
		targetAddress.setZip(source.getAddress().getZip());

		target.setUser(targetUser);
		target.setAddress(targetAddress);

		return target;
	}
	
	private UserResponse convertResponse(UserAddress source, Class<UserResponse> type) {
		UserResponse target = new UserResponse();
		target.setUserId(source.getUser().getUserId());
		target.setName(source.getUser().getName());
		target.setEmail(source.getUser().getEmail());		
		target.setBirthdate(source.getUser().getBirthdate().withOffsetSameInstant(ZoneOffset.of("Z"))
				.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		AddressResponse targetAddres = new AddressResponse();
		targetAddres.setAddressId(source.getAddress().getAddressId());		
		targetAddres.setStreet(source.getAddress().getStreet());
		targetAddres.setState(source.getAddress().getState());
		targetAddres.setCity(source.getAddress().getCity());
		targetAddres.setCountry(source.getAddress().getCountry());
		targetAddres.setZip(source.getAddress().getZip());

		target.setAddress(targetAddres);

		return target;
	}
}