package net.cloudappi.security.rest.dto.user;

import static net.cloudappi.security.rest.utils.UserRestHelper.JsonProperties.ADDRESS;
import static net.cloudappi.security.rest.utils.UserRestHelper.JsonProperties.BIRTHDATE;
import static net.cloudappi.security.rest.utils.UserRestHelper.JsonProperties.EMAIL;
import static net.cloudappi.security.rest.utils.UserRestHelper.JsonProperties.NAME;
import static net.cloudappi.security.rest.utils.UserRestHelper.JsonProperties.USER_ID;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.escalableapps.framework.core.validation.constraints.DatetimeRange;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.ToString;
import net.cloudappi.security.model.SqlConstants;
import net.cloudappi.security.rest.dto.address.AddressResponse;

/**
 * Se registra todos los usuarios<br>
 * <strong>t_user</strong>
 */
@ApiModel
@ToString
public class UserResponse {

	/**
	 * ID AUTOGENERADO<br>
	 * <strong>user_iident bigint NOT NULL</strong>
	 */
	@NotNull
	@Min(1)
	@JsonProperty(value = USER_ID, index = 1, required = true)
	@ApiModelProperty(position = 1, value = "ID AUTOGENERADO", required = true)
	private Long userId;

	/**
	 * Nombre del usuario<br>
	 * <strong>user_vname character varying</strong>
	 */
	@Size(min = 1, max = 100)
	@JsonProperty(value = NAME, index = 2)
	@ApiModelProperty(position = 2, value = "Nombre del usuario")
	private String name;

	/**
	 * Correo electronico del usuario<br>
	 * <strong>user_vemail character varying</strong>
	 */
	@Size(min = 1, max = 200)
	@JsonProperty(value = EMAIL, index = 3)
	@ApiModelProperty(position = 3, value = "Correo electronico del usuario")
	private String email;

	/**
	 * Fecha de cumpleaños del usuario<br>
	 * <strong>user_dbirthdate timestamp without time zone</strong>
	 */
	@DatetimeRange(min = SqlConstants.Limits.TIMESTAMP_MIN, max = SqlConstants.Limits.TIMESTAMP_MAX)
	@JsonProperty(value = BIRTHDATE, index = 4)
	@ApiModelProperty(position = 4, value = "Fecha de cumpleaños del usuario")
	private String birthdate;

	/**
	 * Direccion del usuario<br>
	 * <strong>address AddressResponse</strong>
	 */
	@JsonProperty(value = ADDRESS, index = 5)
	@ApiModelProperty(position = 5, value = "Direccion del usuario")
	private AddressResponse address;

	/**
	 * ID AUTOGENERADO<br>
	 * <strong>user_iident bigint NOT NULL</strong>
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * ID AUTOGENERADO<br>
	 * <strong>user_iident bigint NOT NULL</strong>
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * Nombre del usuario<br>
	 * <strong>user_vname character varying</strong>
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Nombre del usuario<br>
	 * <strong>user_vname character varying</strong>
	 */
	public String getName() {
		return name;
	}

	/**
	 * Correo electronico del usuario<br>
	 * <strong>user_vemail character varying</strong>
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Correo electronico del usuario<br>
	 * <strong>user_vemail character varying</strong>
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Fecha de cumpleaños del usuario<br>
	 * <strong>user_dbirthdate timestamp without time zone</strong>
	 */
	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	/**
	 * Fecha de cumpleaños del usuario<br>
	 * <strong>user_dbirthdate timestamp without time zone</strong>
	 */
	public String getBirthdate() {
		return birthdate;
	}

	public AddressResponse getAddress() {
		return address;
	}

	public void setAddress(AddressResponse address) {
		this.address = address;
	}

}