package net.cloudappi.security.rest.dto.address;

import static net.cloudappi.security.rest.utils.AddressRestHelper.JsonProperties.CITY;
import static net.cloudappi.security.rest.utils.AddressRestHelper.JsonProperties.COUNTRY;
import static net.cloudappi.security.rest.utils.AddressRestHelper.JsonProperties.STATE;
import static net.cloudappi.security.rest.utils.AddressRestHelper.JsonProperties.STREET;
import static net.cloudappi.security.rest.utils.AddressRestHelper.JsonProperties.ZIP;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import com.escalableapps.framework.core.validation.group.Update;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import lombok.ToString;

/**
 * Se registra todos los usuarios<br>
 * <strong>t_addres</strong>
 */
@ToString
public class AddressRequest {

	@JsonIgnore
	@ApiModelProperty(hidden = true)
	private boolean fullUpdate;

	@Min(value = 1, groups = Update.class, message = "No property has been set for update")
	@JsonIgnore
	@ApiModelProperty(hidden = true)
	private int propertyCountForUpdate;

	/**
	 * Nombre de la calle<br>
	 * <strong>addr_vstreet character varying</strong>
	 */
	@Size(min = 1, max = 100)
	@JsonProperty(value = STREET, index = 3)
	@ApiModelProperty(position = 3, value = "Nombre de la calle")
	private String street;

	@ApiModelProperty(hidden = true)
	private boolean streetNull;

	/**
	 * Nombre del estado<br>
	 * <strong>addr_vstate character varying</strong>
	 */
	@Size(min = 1, max = 100)
	@JsonProperty(value = STATE, index = 4)
	@ApiModelProperty(position = 4, value = "Nombre del estado")
	private String state;

	@ApiModelProperty(hidden = true)
	private boolean stateNull;

	/**
	 * Nombre de la ciudad<br>
	 * <strong>addr_vcity character varying</strong>
	 */
	@Size(min = 1, max = 100)
	@JsonProperty(value = CITY, index = 5)
	@ApiModelProperty(position = 5, value = "Nombre de la ciudad")
	private String city;

	@ApiModelProperty(hidden = true)
	private boolean cityNull;

	/**
	 * Nombre del país<br>
	 * <strong>addr_vcountry character varying</strong>
	 */
	@Size(min = 1, max = 100)
	@JsonProperty(value = COUNTRY, index = 6)
	@ApiModelProperty(position = 6, value = "Nombre del país")
	private String country;

	@ApiModelProperty(hidden = true)
	private boolean countryNull;

	/**
	 * Código Postal<br>
	 * <strong>addr_vzip character varying</strong>
	 */
	@Size(min = 1, max = 100)
	@JsonProperty(value = ZIP, index = 7)
	@ApiModelProperty(position = 7, value = "Código Postal")
	private String zip;

	@ApiModelProperty(hidden = true)
	private boolean zipNull;

	public boolean isFullUpdate() {
		return fullUpdate;
	}

	public void setFullUpdate(boolean fullUpdate) {
		this.fullUpdate = fullUpdate;
	}

	public int getPropertyCountForUpdate() {
		return propertyCountForUpdate;
	}

	/**
	 * Nombre de la calle<br>
	 * <strong>addr_vstreet character varying</strong>
	 */
	public void setStreet(String street) {
		this.street = street;
		if (street == null) {
			streetNull = true;
		}
		propertyCountForUpdate++;
	}

	/**
	 * Nombre de la calle<br>
	 * <strong>addr_vstreet character varying</strong>
	 */
	public String getStreet() {
		return street;
	}

	public boolean isStreetNull() {
		return streetNull;
	}

	/**
	 * Nombre del estado<br>
	 * <strong>addr_vstate character varying</strong>
	 */
	public void setState(String state) {
		this.state = state;
		if (state == null) {
			stateNull = true;
		}
		propertyCountForUpdate++;
	}

	/**
	 * Nombre del estado<br>
	 * <strong>addr_vstate character varying</strong>
	 */
	public String getState() {
		return state;
	}

	public boolean isStateNull() {
		return stateNull;
	}

	/**
	 * Nombre de la ciudad<br>
	 * <strong>addr_vcity character varying</strong>
	 */
	public void setCity(String city) {
		this.city = city;
		if (city == null) {
			cityNull = true;
		}
		propertyCountForUpdate++;
	}

	/**
	 * Nombre de la ciudad<br>
	 * <strong>addr_vcity character varying</strong>
	 */
	public String getCity() {
		return city;
	}

	public boolean isCityNull() {
		return cityNull;
	}

	/**
	 * Nombre del país<br>
	 * <strong>addr_vcountry character varying</strong>
	 */
	public void setCountry(String country) {
		this.country = country;
		if (country == null) {
			countryNull = true;
		}
		propertyCountForUpdate++;
	}

	/**
	 * Nombre del país<br>
	 * <strong>addr_vcountry character varying</strong>
	 */
	public String getCountry() {
		return country;
	}

	public boolean isCountryNull() {
		return countryNull;
	}

	/**
	 * Código Postal<br>
	 * <strong>addr_vzip character varying</strong>
	 */
	public void setZip(String zip) {
		this.zip = zip;
		if (zip == null) {
			zipNull = true;
		}
		propertyCountForUpdate++;
	}

	/**
	 * Código Postal<br>
	 * <strong>addr_vzip character varying</strong>
	 */
	public String getZip() {
		return zip;
	}

	public boolean isZipNull() {
		return zipNull;
	}
}