package net.cloudappi.security.rest.dto.user;

import static net.cloudappi.security.rest.utils.UserRestHelper.JsonProperties.ADDRESS;
import static net.cloudappi.security.rest.utils.UserRestHelper.JsonProperties.BIRTHDATE;
import static net.cloudappi.security.rest.utils.UserRestHelper.JsonProperties.EMAIL;
import static net.cloudappi.security.rest.utils.UserRestHelper.JsonProperties.NAME;

import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.escalableapps.framework.core.validation.group.Update;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.ToString;
import net.cloudappi.security.rest.dto.address.AddressRequest;

/**
 * Se registra todos los usuarios<br>
 * <strong>t_user</strong>
 */
@ApiModel
@ToString
public class UserRequest {

	@JsonIgnore
	@ApiModelProperty(hidden = true)
	private boolean fullUpdate;

	@Min(value = 1, groups = Update.class, message = "No property has been set for update")
	@JsonIgnore
	@ApiModelProperty(hidden = true)
	private int propertyCountForUpdate;

	/**
	 * Nombre del usuario<br>
	 * <strong>user_vname character varying</strong>
	 */
	@Size(min = 1, max = 100)
	@JsonProperty(value = NAME, index = 1)
	@ApiModelProperty(position = 1, value = "Nombre del usuario")
	private String name;

	@ApiModelProperty(hidden = true)
	private boolean nameNull;

	/**
	 * Correo electronico del usuario<br>
	 * <strong>user_vemail character varying</strong>
	 */
	@Size(min = 1, max = 200)
	@JsonProperty(value = EMAIL, index = 2)
	@ApiModelProperty(position = 2, value = "Correo electronico del usuario")
	@Pattern(regexp = "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$", message = "Correo inválido, formato permitido nombre@host.dominio, ejemplo alguien@algunlugar.es")
	private String email;

	@ApiModelProperty(hidden = true)
	private boolean emailNull;

	/**
	 * Fecha de cumpleaños del usuario<br>
	 * <strong>user_dbirthdate timestamp without time zone</strong>
	 */
	@JsonProperty(value = BIRTHDATE, index = 3)
	@ApiModelProperty(position = 3, value = "Fecha de cumpleaños del usuario")
	@Pattern(regexp = "^(?:(?:31(\\/)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/)(?:0?[13-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$", message = "Fecha inválida, formato permitido dd/MM/yyyy, ejemplo: 18/02/1995")
	private String birthdate;

	@ApiModelProperty(hidden = true)
	private boolean birthdateNull;

	@JsonProperty(value = ADDRESS, index = 4)
	@ApiModelProperty(position = 4, value = "Direccion del usuario")
	private AddressRequest address;

	public boolean isFullUpdate() {
		return fullUpdate;
	}

	public void setFullUpdate(boolean fullUpdate) {
		this.fullUpdate = fullUpdate;
	}

	public int getPropertyCountForUpdate() {
		return propertyCountForUpdate;
	}

	/**
	 * Nombre del usuario<br>
	 * <strong>user_vname character varying</strong>
	 */
	public void setName(String name) {
		this.name = name;
		if (name == null) {
			nameNull = true;
		}
		propertyCountForUpdate++;
	}

	/**
	 * Nombre del usuario<br>
	 * <strong>user_vname character varying</strong>
	 */
	public String getName() {
		return name;
	}

	public boolean isNameNull() {
		return nameNull;
	}

	/**
	 * Correo electronico del usuario<br>
	 * <strong>user_vemail character varying</strong>
	 */
	public void setEmail(String email) {
		this.email = email;
		if (email == null) {
			emailNull = true;
		}
		propertyCountForUpdate++;
	}

	/**
	 * Correo electronico del usuario<br>
	 * <strong>user_vemail character varying</strong>
	 */
	public String getEmail() {
		return email;
	}

	public boolean isEmailNull() {
		return emailNull;
	}

	/**
	 * Fecha de cumpleaños del usuario<br>
	 * <strong>user_dbirthdate timestamp without time zone</strong>
	 */
	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
		if (birthdate == null) {
			birthdateNull = true;
		}
		propertyCountForUpdate++;
	}

	/**
	 * Fecha de cumpleaños del usuario<br>
	 * <strong>user_dbirthdate timestamp without time zone</strong>
	 */
	public String getBirthdate() {
		return birthdate;
	}

	public boolean isBirthdateNull() {
		return birthdateNull;
	}

	public AddressRequest getAddress() {
		return address;
	}

	public void setAddress(AddressRequest address) {
		this.address = address;
	}

}