package net.cloudappi.security.rest.utils;

public class AddressRestHelper {

	public static class JsonProperties {
		public static final String ADDRESS_ID = "id";
		public static final String USER_ID = "userId";
		public static final String STREET = "street";
		public static final String STATE = "state";
		public static final String CITY = "city";
		public static final String COUNTRY = "country";
		public static final String ZIP = "zip";
	}
}