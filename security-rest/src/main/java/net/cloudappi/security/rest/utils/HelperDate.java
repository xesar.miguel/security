package net.cloudappi.security.rest.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HelperDate {

	public static Date stringToDate(String value) {
		SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
		try {
			return date.parse(value);
		} catch (ParseException e) {
			return null;
		}
	}

}
