package net.cloudappi.security.rest.converter.user;

import java.time.ZoneOffset;

import com.escalableapps.framework.core.conversion.ea.PojoConverter;

import net.cloudappi.security.model.vo.AddressUpdate;
import net.cloudappi.security.model.vo.UserAddressUpdate;
import net.cloudappi.security.model.vo.UserUpdate;
import net.cloudappi.security.rest.dto.user.UserRequest;
import net.cloudappi.security.rest.utils.HelperDate;

public class UserRequest2UserUpdate implements PojoConverter<UserRequest, UserAddressUpdate> {

	@Override
	public UserAddressUpdate convert(UserRequest source, Class<UserAddressUpdate> type) {
		
		boolean fullUpdate = source.isFullUpdate();
		
		UserAddressUpdate target = new UserAddressUpdate();

		UserUpdate targetUser = new UserUpdate();
		
		if (fullUpdate || source.getName() != null || source.isNameNull()) {
			targetUser.setName(source.getName());
		}
		if (fullUpdate || source.getEmail() != null || source.isEmailNull()) {
			targetUser.setEmail(source.getEmail());
		}
		if (fullUpdate || source.getBirthdate() != null || source.isBirthdateNull()) {
			targetUser.setBirthdate(HelperDate.stringToDate(source.getBirthdate()).toInstant()
				      .atZone(ZoneOffset.of("Z")).toOffsetDateTime());
		}
		
		AddressUpdate targetAddress = new AddressUpdate();

		if (fullUpdate || source.getAddress().getStreet() != null || source.getAddress().isStreetNull()) {
			targetAddress.setStreet(source.getAddress().getStreet());
		}
		if (fullUpdate || source.getAddress().getState() != null || source.getAddress().isStateNull()) {
			targetAddress.setState(source.getAddress().getState());
		}
		if (fullUpdate || source.getAddress().getCity() != null || source.getAddress().isCityNull()) {
			targetAddress.setCity(source.getAddress().getCity());
		}
		if (fullUpdate || source.getAddress().getCountry() != null || source.getAddress().isCountryNull()) {
			targetAddress.setCountry(source.getAddress().getCountry());
		}
		if (fullUpdate || source.getAddress().getZip() != null || source.getAddress().isZipNull()) {
			targetAddress.setZip(source.getAddress().getZip());
		}
						
		target.setUser(targetUser);
		target.setAddress(targetAddress);
		
		return target;
	}

	@Override
	public boolean canConvert(Class<?> sourceClass, Class<?> targetClass) {
		return UserRequest.class.equals(sourceClass) && UserAddressUpdate.class.equals(targetClass);
	}
}