package net.cloudappi.security.rest.converter.user;

import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import com.escalableapps.framework.core.conversion.ea.PojoConverter;

import net.cloudappi.security.model.vo.UserAddress;
import net.cloudappi.security.rest.dto.address.AddressResponse;
import net.cloudappi.security.rest.dto.user.UserResponse;

public class User2UserResponse implements PojoConverter<UserAddress, UserResponse> {

	@Override
	public UserResponse convert(UserAddress source, Class<UserResponse> type) {
		UserResponse target = new UserResponse();
		target.setUserId(source.getUser().getUserId());
		target.setName(source.getUser().getName());
		target.setEmail(source.getUser().getEmail());		
		target.setBirthdate(source.getUser().getBirthdate().withOffsetSameInstant(ZoneOffset.of("Z"))
				.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		AddressResponse targetAddres = new AddressResponse();
		targetAddres.setAddressId(source.getAddress().getAddressId());		
		targetAddres.setStreet(source.getAddress().getStreet());
		targetAddres.setState(source.getAddress().getState());
		targetAddres.setCity(source.getAddress().getCity());
		targetAddres.setCountry(source.getAddress().getCountry());
		targetAddres.setZip(source.getAddress().getZip());

		target.setAddress(targetAddres);

		return target;
	}

	@Override
	public boolean canConvert(Class<?> sourceClass, Class<?> targetClass) {
		return UserAddress.class.equals(sourceClass) && UserResponse.class.equals(targetClass);
	}

}