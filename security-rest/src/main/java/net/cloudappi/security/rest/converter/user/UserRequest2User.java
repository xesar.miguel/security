package net.cloudappi.security.rest.converter.user;

import java.time.ZoneOffset;

import com.escalableapps.framework.core.conversion.ea.PojoConverter;

import net.cloudappi.security.model.entity.Address;
import net.cloudappi.security.model.entity.User;
import net.cloudappi.security.model.vo.UserAddress;
import net.cloudappi.security.rest.dto.user.UserRequest;
import net.cloudappi.security.rest.utils.HelperDate;

public class UserRequest2User implements PojoConverter<UserRequest, UserAddress> {

	@Override
	public UserAddress convert(UserRequest source, Class<UserAddress> type) {

		UserAddress target = new UserAddress();

		User targetUser = new User();
		targetUser.setName(source.getName());
		targetUser.setEmail(source.getEmail());
		targetUser.setBirthdate(HelperDate.stringToDate(source.getBirthdate()).toInstant().atZone(ZoneOffset.of("Z"))
				.toOffsetDateTime());

		Address targetAddress = new Address();
		targetAddress.setStreet(source.getAddress().getStreet());
		targetAddress.setState(source.getAddress().getState());
		targetAddress.setCity(source.getAddress().getCity());
		targetAddress.setCountry(source.getAddress().getCountry());
		targetAddress.setZip(source.getAddress().getZip());

		target.setUser(targetUser);
		target.setAddress(targetAddress);

		return target;
	}

	@Override
	public boolean canConvert(Class<?> sourceClass, Class<?> targetClass) {
		return UserRequest.class.equals(sourceClass) && UserAddress.class.equals(targetClass);
	}

}