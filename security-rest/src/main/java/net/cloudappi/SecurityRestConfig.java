package net.cloudappi;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import net.cloudappi.security.rest.converter.user.User2UserResponse;
import net.cloudappi.security.rest.converter.user.UserRequest2User;
import net.cloudappi.security.rest.converter.user.UserRequest2UserUpdate;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SecurityRestConfig {

	@Bean
	public User2UserResponse user2UserResponse() {
		return new User2UserResponse();
	}

	@Bean
	public UserRequest2User userRequest2User() {
		return new UserRequest2User();
	}

	@Bean
	public UserRequest2UserUpdate userRequest2UserUpdate() {
		return new UserRequest2UserUpdate();
	}

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2) //
				.select() //
				.apis(RequestHandlerSelectors.any()) //
				.paths(PathSelectors.ant("/api/**")) //
				.build();
	}
}
