package net.cloudappi.security.jpa.converter.user;

import javax.persistence.*;
import javax.persistence.criteria.*;

import org.hibernate.*;
import org.hibernate.query.Query;
import org.springframework.stereotype.*;

import net.cloudappi.security.jpa.persistence.*;
import net.cloudappi.security.model.vo.*;
import com.escalableapps.framework.core.conversion.ea.*;

@Component
public class UserUpdate2Query implements PojoConverter<UserUpdate, Query<UserEntity>> {

	private final EntityManager entityManager;

	public UserUpdate2Query(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Query<UserEntity> convert(UserUpdate source, Class<Query<UserEntity>> type) {
		Session session = entityManager.unwrap(Session.class);
		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		CriteriaUpdate<UserEntity> update = criteriaBuilder.createCriteriaUpdate(UserEntity.class);
		Root<UserEntity> root = update.from(UserEntity.class);
		update.set(root.get("name"), source.getName());
		update.set(root.get("email"), source.getEmail());
		update.set(root.get("birthdate"), source.getBirthdate());		
		update.where(criteriaBuilder.equal(root.get("userId"), source.getUserId()));
		return session.createQuery(update);
	}

	@Override
	public boolean canConvert(Class<?> sourceClass, Class<?> targetClass) {
		return UserUpdate.class.equals(sourceClass) && Query.class.equals(targetClass);
	}
}