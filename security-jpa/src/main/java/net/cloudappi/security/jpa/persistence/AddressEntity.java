package net.cloudappi.security.jpa.persistence;

import static net.cloudappi.security.model.SqlConstants.Types.BIGINT;
import static net.cloudappi.security.model.SqlConstants.Types.VARCHAR;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;

import com.escalableapps.framework.core.validation.group.Create;
import com.escalableapps.framework.core.validation.group.Update;

/**
 * Se registra todos los usuarios<br>
 * <strong>t_addres</strong>
 */
@Entity
@Table(schema = "public", name = "t_addres")
public class AddressEntity {

	/**
	 * ID AUTOGENERADO<br>
	 * <strong>addr_iident bigint NOT NULL</strong>
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Null(groups = { Create.class })
	@NotNull(groups = { Update.class })
	@Min(1)
	@Column(name = "addr_iident", columnDefinition = BIGINT)
	private Long addressId;

	/**
	 * Nombre del usuario<br>
	 * <strong>user_iident bigint</strong>
	 */
	@Min(1)
	@Column(name = "user_iident", columnDefinition = BIGINT)
	private Long userId;

	/**
	 * Nombre de la calle<br>
	 * <strong>addr_vstreet character varying</strong>
	 */
	@Size(min = 1, max = 100)
	@Column(name = "addr_vstreet", columnDefinition = VARCHAR)
	private String street;

	/**
	 * Nombre del estado<br>
	 * <strong>addr_vstate character varying</strong>
	 */
	@Size(min = 1, max = 100)
	@Column(name = "addr_vstate", columnDefinition = VARCHAR)
	private String state;

	/**
	 * Nombre de la ciudad<br>
	 * <strong>addr_vcity character varying</strong>
	 */
	@Size(min = 1, max = 100)
	@Column(name = "addr_vcity", columnDefinition = VARCHAR)
	private String city;

	/**
	 * Nombre del país<br>
	 * <strong>addr_vcountry character varying</strong>
	 */
	@Size(min = 1, max = 100)
	@Column(name = "addr_vcountry", columnDefinition = VARCHAR)
	private String country;

	/**
	 * Código Postal<br>
	 * <strong>addr_vzip character varying</strong>
	 */
	@Size(min = 1, max = 100)
	@Column(name = "addr_vzip", columnDefinition = VARCHAR)
	private String zip;

	/**
	 * ID AUTOGENERADO<br>
	 * <strong>addr_iident bigint NOT NULL</strong>
	 */
	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}

	/**
	 * ID AUTOGENERADO<br>
	 * <strong>addr_iident bigint NOT NULL</strong>
	 */
	public Long getAddressId() {
		return addressId;
	}

	/**
	 * Nombre del usuario<br>
	 * <strong>user_iident bigint</strong>
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * Nombre del usuario<br>
	 * <strong>user_iident bigint</strong>
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * Nombre de la calle<br>
	 * <strong>addr_vstreet character varying</strong>
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * Nombre de la calle<br>
	 * <strong>addr_vstreet character varying</strong>
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * Nombre del estado<br>
	 * <strong>addr_vstate character varying</strong>
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Nombre del estado<br>
	 * <strong>addr_vstate character varying</strong>
	 */
	public String getState() {
		return state;
	}

	/**
	 * Nombre de la ciudad<br>
	 * <strong>addr_vcity character varying</strong>
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Nombre de la ciudad<br>
	 * <strong>addr_vcity character varying</strong>
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Nombre del país<br>
	 * <strong>addr_vcountry character varying</strong>
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Nombre del país<br>
	 * <strong>addr_vcountry character varying</strong>
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Código Postal<br>
	 * <strong>addr_vzip character varying</strong>
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * Código Postal<br>
	 * <strong>addr_vzip character varying</strong>
	 */
	public String getZip() {
		return zip;
	}
}