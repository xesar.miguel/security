package net.cloudappi.security.jpa.converter.address;

import org.springframework.stereotype.Component;

import com.escalableapps.framework.core.conversion.ea.PojoConverter;

import net.cloudappi.security.jpa.persistence.AddressEntity;
import net.cloudappi.security.model.entity.Address;

@Component
public class Address2AddressEntity implements PojoConverter<Address, AddressEntity> {

  @Override
  public AddressEntity convert(Address source, Class<AddressEntity> type) {
    AddressEntity target = new AddressEntity();
    target.setAddressId(source.getAddressId());
    target.setUserId(source.getUserId());
    target.setStreet(source.getStreet());
    target.setState(source.getState());
    target.setCity(source.getCity());
    target.setCountry(source.getCountry());
    target.setZip(source.getZip());
    return target;
  }

  @Override
  public boolean canConvert(Class<?> sourceClass, Class<?> targetClass) {
    return Address.class.equals(sourceClass) && AddressEntity.class.equals(targetClass);
  }

}