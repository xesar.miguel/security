package net.cloudappi.security.jpa.persistence;

import static net.cloudappi.security.model.SqlConstants.Types.BIGINT;
import static net.cloudappi.security.model.SqlConstants.Types.TIMESTAMP;
import static net.cloudappi.security.model.SqlConstants.Types.VARCHAR;

import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;

import com.escalableapps.framework.core.validation.group.Create;
import com.escalableapps.framework.core.validation.group.Update;

/**
 * Se registra todos los usuarios<br>
 * <strong>t_user</strong>
 */
@Entity
@Table(schema = "public", name = "t_user")
public class UserEntity {

	/**
	 * ID AUTOGENERADO<br>
	 * <strong>user_iident bigint NOT NULL</strong>
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Null(groups = { Create.class })
	@NotNull(groups = { Update.class })
	@Min(1)
	@Column(name = "user_iident", columnDefinition = BIGINT)
	private Long userId;

	/**
	 * Nombre del usuario<br>
	 * <strong>user_vname character varying</strong>
	 */
	@Size(min = 1, max = 100)
	@Column(name = "user_vname", columnDefinition = VARCHAR)
	private String name;

	/**
	 * Correo electronico del usuario<br>
	 * <strong>user_vemail character varying</strong>
	 */
	@Size(min = 1, max = 200)
	@Column(name = "user_vemail", columnDefinition = VARCHAR)
	private String email;

	/**
	 * Fecha de cumpleaños del usuario<br>
	 * <strong>user_dbirthdate timestamp without time zone</strong>
	 */
	@Column(name = "user_dbirthdate", columnDefinition = TIMESTAMP)
	private OffsetDateTime birthdate;

	/**
	 * ID AUTOGENERADO<br>
	 * <strong>user_iident bigint NOT NULL</strong>
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * ID AUTOGENERADO<br>
	 * <strong>user_iident bigint NOT NULL</strong>
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * Nombre del usuario<br>
	 * <strong>user_vname character varying</strong>
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Nombre del usuario<br>
	 * <strong>user_vname character varying</strong>
	 */
	public String getName() {
		return name;
	}

	/**
	 * Correo electronico del usuario<br>
	 * <strong>user_vemail character varying</strong>
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Correo electronico del usuario<br>
	 * <strong>user_vemail character varying</strong>
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Fecha de cumpleaños del usuario<br>
	 * <strong>user_dbirthdate timestamp without time zone</strong>
	 */
	public void setBirthdate(OffsetDateTime birthdate) {
		this.birthdate = birthdate;
	}

	/**
	 * Fecha de cumpleaños del usuario<br>
	 * <strong>user_dbirthdate timestamp without time zone</strong>
	 */
	public OffsetDateTime getBirthdate() {
		return birthdate;
	}
}