package net.cloudappi.security.jpa.converter.address;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;

import com.escalableapps.framework.core.conversion.ea.PojoConverter;

import net.cloudappi.security.jpa.persistence.AddressEntity;
import net.cloudappi.security.model.vo.AddressUpdate;

@Component
public class AddressUpdate2Query implements PojoConverter<AddressUpdate, Query<AddressEntity>> {

	private final EntityManager entityManager;

	public AddressUpdate2Query(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Query<AddressEntity> convert(AddressUpdate source, Class<Query<AddressEntity>> type) {
		Session session = entityManager.unwrap(Session.class);
		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		CriteriaUpdate<AddressEntity> update = criteriaBuilder.createCriteriaUpdate(AddressEntity.class);
		Root<AddressEntity> root = update.from(AddressEntity.class);
		update.set(root.get("userId"), source.getUserId());
		update.set(root.get("street"), source.getStreet());
		update.set(root.get("state"), source.getState());
		update.set(root.get("city"), source.getCity());
		update.set(root.get("country"), source.getCountry());
		update.set(root.get("zip"), source.getZip());
		update.where(criteriaBuilder.equal(root.get("userId"), source.getUserId()));
		return session.createQuery(update);
	}

	@Override
	public boolean canConvert(Class<?> sourceClass, Class<?> targetClass) {
		return AddressUpdate.class.equals(sourceClass) && Query.class.equals(targetClass);
	}
}