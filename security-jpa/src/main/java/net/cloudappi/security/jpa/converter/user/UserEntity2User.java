package net.cloudappi.security.jpa.converter.user;

import org.springframework.stereotype.Component;

import com.escalableapps.framework.core.conversion.ea.PojoConverter;

import net.cloudappi.security.jpa.persistence.UserEntity;
import net.cloudappi.security.model.entity.User;

@Component
public class UserEntity2User implements PojoConverter<UserEntity, User> {

  @Override
  public User convert(UserEntity source, Class<User> type) {
    User target = new User();
    target.setUserId(source.getUserId());
    target.setName(source.getName());
    target.setEmail(source.getEmail());
    target.setBirthdate(source.getBirthdate());
    return target;
  }

  @Override
  public boolean canConvert(Class<?> sourceClass, Class<?> targetClass) {
    return UserEntity.class.equals(sourceClass) && User.class.equals(targetClass);
  }

}