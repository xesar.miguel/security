package net.cloudappi.security.jpa.converter.user;

import org.springframework.stereotype.Component;

import com.escalableapps.framework.core.conversion.ea.PojoConverter;

import net.cloudappi.security.jpa.persistence.UserEntity;
import net.cloudappi.security.model.entity.User;

@Component
public class User2UserEntity implements PojoConverter<User, UserEntity> {

  @Override
  public UserEntity convert(User source, Class<UserEntity> type) {
    UserEntity target = new UserEntity();
    target.setUserId(source.getUserId());
    target.setName(source.getName());
    target.setEmail(source.getEmail());
    target.setBirthdate(source.getBirthdate());
    return target;
  }

  @Override
  public boolean canConvert(Class<?> sourceClass, Class<?> targetClass) {
    return User.class.equals(sourceClass) && UserEntity.class.equals(targetClass);
  }

}