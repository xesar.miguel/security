package net.cloudappi.security.jpa.port;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import com.escalableapps.framework.core.service.ConversionService;

import lombok.extern.slf4j.Slf4j;
import net.cloudappi.security.jpa.persistence.UserEntity;
import net.cloudappi.security.model.entity.User;
import net.cloudappi.security.model.port.UserPort;
import net.cloudappi.security.model.vo.UserUpdate;

@Slf4j
@Repository
@Validated
public class UserJpa implements UserPort {

	public interface UserRepository extends JpaRepository<UserEntity, Long> {

	}

	private final UserRepository userRepository;
	private final ConversionService conversionService;

	public UserJpa(UserRepository userRepository, ConversionService conversionService) {
		this.userRepository = userRepository;
		this.conversionService = conversionService;
	}

	@Override
	public User createUser(User userToCreate) {
		log.debug("userToCreate={}", userToCreate);

		UserEntity userEntity = userRepository.save(conversionService.convert(userToCreate, UserEntity.class));

		User user = conversionService.convert(userEntity, User.class);
		log.debug("user={}", user);
		return user;
	}

	@Override
	public User findUser(Long userId) {
		log.debug("userId={}", userId);

		UserEntity userEntity = userRepository.findById(userId).orElse(null);

		User user = conversionService.convert(userEntity, User.class);
		log.debug("user={}", user);
		return user;
	}

	@Override
	public List<User> findUsers() {
		List<UserEntity> page = userRepository.findAll();
		List<User> pageResponse = conversionService.convert(page, User.class);
		log.debug("pageResponse={}", pageResponse);
		return pageResponse;
	}

	@Override
	public boolean existsUser(Long userId) {
		log.debug("userId={}", userId);

		boolean exists = userRepository.existsById(userId);

		log.debug("exists={}", exists);
		return exists;
	}

	@Override
	public long updateUser(UserUpdate userUpdate) {
		log.debug("userUpdate={}", userUpdate);
		org.hibernate.query.Query<UserEntity> update = conversionService.convert(userUpdate,
				org.hibernate.query.Query.class);

		long updateCount = update.executeUpdate();

		return updateCount;
	}

	@Override
	public void deleteUser(Long userId) {
		log.debug("userId={}", userId);
		userRepository.deleteById(userId);
	}
}
