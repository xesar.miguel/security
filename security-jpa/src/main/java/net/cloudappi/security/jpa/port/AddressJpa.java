package net.cloudappi.security.jpa.port;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import com.escalableapps.framework.core.service.ConversionService;

import lombok.extern.slf4j.Slf4j;
import net.cloudappi.security.jpa.persistence.AddressEntity;
import net.cloudappi.security.model.entity.Address;
import net.cloudappi.security.model.port.AddressPort;
import net.cloudappi.security.model.vo.AddressUpdate;

@Slf4j
@Repository
@Validated
public class AddressJpa implements AddressPort {

	public interface AddressRepository extends JpaRepository<AddressEntity, Long> {
		void deleteByUserId(Long userId);
		AddressEntity findByUserId(Long userId);
	}

	private final AddressRepository addressRepository;
	private final ConversionService conversionService;

	public AddressJpa(AddressRepository addressRepository, ConversionService conversionService) {
		this.addressRepository = addressRepository;
		this.conversionService = conversionService;
	}

	@Override
	public Address createAddress(Address addressToCreate) {
		log.debug("addressToCreate={}", addressToCreate);

		AddressEntity addressEntity = addressRepository
				.save(conversionService.convert(addressToCreate, AddressEntity.class));

		Address address = conversionService.convert(addressEntity, Address.class);
		log.debug("address={}", address);
		return address;
	}

	@Override
	public Address findAddress(Long userId) {
		log.debug("userId={}", userId);

		AddressEntity addressEntity = addressRepository.findByUserId(userId);

		Address address = conversionService.convert(addressEntity, Address.class);
		log.debug("address={}", address);
		return address;
	}

	@Override
	public long updateAddress(AddressUpdate addressUpdate) {
		log.debug("addressUpdate={}", addressUpdate);
		org.hibernate.query.Query<AddressEntity> update = conversionService.convert(addressUpdate,
				org.hibernate.query.Query.class);

		long updateCount = update.executeUpdate();

		return updateCount;
	}

	@Override
	public void deleteAddress(Long userId) {
		log.debug("userId={}", userId);
		addressRepository.deleteByUserId(userId);
	}
}
