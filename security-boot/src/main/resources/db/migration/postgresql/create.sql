CREATE TABLE public.t_user (
	user_iident bigserial NOT NULL, -- ID¶@cmdb{"seq":"t_user_user_iident_seq","name":"id"}
	user_vname varchar(100) NULL, -- Nombre del usuario¶@cmdb{"name":"userVname"}
	user_vemail varchar(200) NULL, -- Correo electronico del usuario¶@cmdb{"name":"userVemail"}
	user_dbirthdate timestamp NULL, -- Fecha de cumpleaños del usuario¶@cmdb{"name":"userDbirthdate"}
	CONSTRAINT pk_user PRIMARY KEY (user_iident)
);
COMMENT ON TABLE public.t_user IS 'Se registra todos los usuarios
@cmdb{"name":"TUser"}';

-- Column comments

COMMENT ON COLUMN public.t_user.user_iident IS 'ID
@cmdb{"seq":"t_user_user_iident_seq","name":"id"}';
COMMENT ON COLUMN public.t_user.user_vname IS 'Nombre del usuario
@cmdb{"name":"userVname"}';
COMMENT ON COLUMN public.t_user.user_vemail IS 'Correo electronico del usuario
@cmdb{"name":"userVemail"}';
COMMENT ON COLUMN public.t_user.user_dbirthdate IS 'Fecha de cumpleaños del usuario
@cmdb{"name":"userDbirthdate"}';

CREATE TABLE public.t_addres (
	addr_iident bigserial NOT NULL, -- ID¶@cmdb{"seq":"t_addres_addr_iident_seq","name":"id"}
	user_iident int8 NULL, -- Nombre del usuario¶@cmdb{"name":"userIident"}
	addr_vstreet varchar(100) NULL, -- Nombre de la calle¶@cmdb{"name":"addrVstreet"}
	addr_vstate varchar(100) NULL, -- Nombre del estado¶@cmdb{"name":"addrVstate"}
	addr_vcity varchar(100) NULL, -- Nombre de la ciudad¶@cmdb{"name":"addrVcity"}
	addr_vcountry varchar(100) NULL, -- Nombre del país¶@cmdb{"name":"addrVcountry"}
	addr_vzip varchar(100) NULL, -- Código Postal¶@cmdb{"name":"addrVzip"}
	CONSTRAINT pk_addres PRIMARY KEY (addr_iident),
	CONSTRAINT fk_addres_user FOREIGN KEY (user_iident) REFERENCES t_user(user_iident)
);
COMMENT ON TABLE public.t_addres IS 'Se registra todos los usuarios
@cmdb{"name":"TAddres"}';

-- Column comments

COMMENT ON COLUMN public.t_addres.addr_iident IS 'ID
@cmdb{"seq":"t_addres_addr_iident_seq","name":"id"}';
COMMENT ON COLUMN public.t_addres.user_iident IS 'Nombre del usuario
@cmdb{"name":"userIident"}';
COMMENT ON COLUMN public.t_addres.addr_vstreet IS 'Nombre de la calle
@cmdb{"name":"addrVstreet"}';
COMMENT ON COLUMN public.t_addres.addr_vstate IS 'Nombre del estado
@cmdb{"name":"addrVstate"}';
COMMENT ON COLUMN public.t_addres.addr_vcity IS 'Nombre de la ciudad
@cmdb{"name":"addrVcity"}';
COMMENT ON COLUMN public.t_addres.addr_vcountry IS 'Nombre del país
@cmdb{"name":"addrVcountry"}';
COMMENT ON COLUMN public.t_addres.addr_vzip IS 'Código Postal
@cmdb{"name":"addrVzip"}';
