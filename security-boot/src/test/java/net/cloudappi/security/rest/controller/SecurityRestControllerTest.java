package net.cloudappi.security.rest.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.escalableapps.framework.core.service.ConversionService;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.cloudappi.security.model.entity.Address;
import net.cloudappi.security.model.entity.User;
import net.cloudappi.security.model.service.OrquestadorService;
import net.cloudappi.security.model.vo.AddressUpdate;
import net.cloudappi.security.model.vo.UserAddress;
import net.cloudappi.security.model.vo.UserAddressUpdate;
import net.cloudappi.security.model.vo.UserUpdate;
import net.cloudappi.security.rest.dto.address.AddressRequest;
import net.cloudappi.security.rest.dto.address.AddressResponse;
import net.cloudappi.security.rest.dto.user.UserRequest;
import net.cloudappi.security.rest.dto.user.UserResponse;
import net.cloudappi.security.rest.utils.HelperDate;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
public class SecurityRestControllerTest {

	@Mock
	private OrquestadorService orquestadorService;

	@Mock
	private ConversionService conversionService;

	private MockMvc mockMvc;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.standaloneSetup(new SecurityRestController(orquestadorService, conversionService))
				.build();
	}

	@Test
	public void getUsers() throws Exception {

		// Data
		List<UserAddress> userAddress = this.getListUserAddres();
		List<UserResponse> userResponse = this.getListUserResponse();

		// When
		when(orquestadorService.findUsers()).thenReturn(userAddress);
		when(conversionService.convert(userAddress, UserResponse.class)).thenReturn(userResponse);

		mockMvc.perform(get("/api/v1/users")).andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].id", is(1))).andExpect(jsonPath("$[0].name", is("miguel")));

		verify(orquestadorService, times(1)).findUsers();
		verifyNoMoreInteractions(orquestadorService);
	}

	@Test
	public void getUserById() throws Exception {
		// Data
		UserAddress userAddress = this.getUserAddres();
		UserResponse userResponse = this.getUserResponse();

		// When
		when(orquestadorService.findUser(Long.valueOf(1))).thenReturn(userAddress);
		when(conversionService.convert(userAddress, UserResponse.class)).thenReturn(userResponse);

		mockMvc.perform(get("/api/v1/user/{userId}", userAddress.getUser().getUserId())).andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(1))).andExpect(jsonPath("$.name", is("miguel")));

		verify(orquestadorService, times(1)).findUser(Long.valueOf(1));
		verifyNoMoreInteractions(orquestadorService);

	}

	@Test
	public void getUserById404NotFound() throws Exception {
		// Data
		UserAddress userAddress = this.getUserAddres();

		// When
		when(orquestadorService.findUser(Long.valueOf(1))).thenReturn(null);

		mockMvc.perform(get("/api/v1/user/{userId}", userAddress.getUser().getUserId()))
				.andExpect(status().isNotFound());

		verify(orquestadorService, times(1)).findUser(Long.valueOf(1));
		verifyNoMoreInteractions(orquestadorService);

	}

	@Test
	public void putUser() throws Exception {

		Long id = Long.valueOf(1);

		UserAddress userAddress = this.getUserAddres();
		UserRequest userRequest = this.getUserRequest();
		UserAddressUpdate userAddressUpdate = this.getUserAddresUpdate(id);

		when(orquestadorService.findUser(Long.valueOf(1))).thenReturn(userAddress);
		when(orquestadorService.existsUser(id)).thenReturn(true);
		when(orquestadorService.updateUser(userAddressUpdate.getUser(), userAddressUpdate.getAddress())).thenReturn(id);

		mockMvc.perform(put("/api/v1/user/{userId}", id).contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(userRequest))).andExpect(status().isOk());

	}

	@Test
	public void putUser404NotFound() throws Exception {

		Long id = Long.valueOf(1);
		UserRequest userRequest = this.getUserRequest();

		when(orquestadorService.existsUser(id)).thenReturn(false);

		mockMvc.perform(put("/api/v1/user/{userId}", id).contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(userRequest))).andExpect(status().isNotFound());

		verify(orquestadorService, times(1)).existsUser(id);
		verifyNoMoreInteractions(orquestadorService);
	}

	@Test
	public void deleteUserById() throws Exception {
		UserAddress userAddress = this.getUserAddres();

		doNothing().when(orquestadorService).deleteUser(userAddress.getUser().getUserId());

		mockMvc.perform(delete("/api/v1/user/{id}", userAddress.getUser().getUserId())).andExpect(status().isOk());

		verify(orquestadorService, times(1)).deleteUser(userAddress.getUser().getUserId());
		verifyNoMoreInteractions(orquestadorService);
	}

	private UserAddress getUserAddres() {

		UserAddress userAddress = new UserAddress();
		User user = new User();
		user.setUserId(Long.valueOf(1));
		user.setName("miguel");
		user.setEmail("miguel@gmail.com");
		user.setBirthdate(
				HelperDate.stringToDate("18/03/1990").toInstant().atZone(ZoneOffset.of("Z")).toOffsetDateTime());
		Address address = new Address();
		address.setAddressId(Long.valueOf(1));
		address.setCity("Lima");
		address.setCountry("Lima");
		address.setState("Lima");
		address.setStreet("Av. 3 Octubre");
		address.setZip("051");

		userAddress.setUser(user);
		userAddress.setAddress(address);
		
		return userAddress;
	}

	private UserAddressUpdate getUserAddresUpdate(Long id) {

		UserAddressUpdate userAddress = new UserAddressUpdate();

		UserRequest userRequest = getUserRequest();

		UserUpdate user = new UserUpdate();
		user.setUserId(id);
		user.setName(userRequest.getName());
		user.setEmail(userRequest.getEmail());
		user.setBirthdate(HelperDate.stringToDate(userRequest.getBirthdate()).toInstant().atZone(ZoneOffset.of("Z"))
				.toOffsetDateTime());
		AddressUpdate address = new AddressUpdate();

		address.setUserId(id);
		address.setCity(userRequest.getAddress().getCity());
		address.setCountry(userRequest.getAddress().getCountry());
		address.setState(userRequest.getAddress().getState());
		address.setStreet(userRequest.getAddress().getStreet());
		address.setZip(userRequest.getAddress().getZip());

		userAddress.setUser(user);
		userAddress.setAddress(address);

		return userAddress;
	}

	private UserRequest getUserRequest() {

		UserRequest userRequest = new UserRequest();

		userRequest.setName("miguel");
		userRequest.setEmail("miguel@gmail.com");
		userRequest.setBirthdate("18/03/1990");
		AddressRequest address = new AddressRequest();

		address.setCity("Lima");
		address.setCountry("Lima");
		address.setState("Lima");
		address.setStreet("Av. 3 Octubre");
		address.setZip("051");

		userRequest.setAddress(address);

		return userRequest;
	}

	private UserResponse getUserResponse() {

		UserAddress userAddress = this.getUserAddres();

		// Data conversionService.conver
		UserResponse userResponse = new UserResponse();
		userResponse.setUserId(userAddress.getUser().getUserId());
		userResponse.setName(userAddress.getUser().getName());
		userResponse.setEmail(userAddress.getUser().getEmail());
		userResponse.setBirthdate(userAddress.getUser().getBirthdate().withOffsetSameInstant(ZoneOffset.of("Z"))
				.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		AddressResponse targetAddres = new AddressResponse();
		targetAddres.setAddressId(userAddress.getAddress().getAddressId());
		targetAddres.setStreet(userAddress.getAddress().getStreet());
		targetAddres.setState(userAddress.getAddress().getState());
		targetAddres.setCity(userAddress.getAddress().getCity());
		targetAddres.setCountry(userAddress.getAddress().getCountry());
		targetAddres.setZip(userAddress.getAddress().getZip());
		userResponse.setAddress(targetAddres);

		return userResponse;
	}

	private List<UserAddress> getListUserAddres() {
		List<UserAddress> list = new ArrayList<>();
		list.add(getUserAddres());
		return list;
	}

	private List<UserResponse> getListUserResponse() {
		List<UserResponse> list = new ArrayList<>();
		list.add(getUserResponse());
		return list;
	}

	private static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
