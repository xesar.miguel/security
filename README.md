# Servicio de Seguridad de Usuarios
Servicio para la creación, actualización, búsqueda y eliminación de usuarios.
## Estructura de datos
Las entidades utilizadas se definen en el siguiente diagrama de entidad y relación.

![DER](https://gitlab.com/xesar.miguel/security/-/raw/dev/security-boot/src/main/resources/db/migration/postgresql/ER.png)
-   Un Usuario registra a una persona en la aplicación:
    -   ID (user_iident) Se auto-genera desde la aplicación
    -   Nombre (user_vname) Nombre del usuario
    -   Email(user_vemail) Email del usuario
    -   Fecha de Nacimiento (user_dbirthdate) Fecha de Nacimiento del usuario
-   Una dirección se registra asociando un usuario :
    -   ID (addr_iident) Se auto-genera desde la aplicación
    -   User ID (user_iident) ID del usuario asociado
    -   Dirección (addr_vstreet) Dirección
    -   Estado (addr_vstate) Nombre del estado
    -   Ciudad (addr_vcity) Nombre de la ciudad
    -   Pais (addr_vcountry) Nombre del País
    -   Código Postal (addr_vzip) Código Postal

El script para generar las tablas en la base de datos se encuentra en el siguiente link

[BD](https://gitlab.com/xesar.miguel/security/-/blob/master/security-boot/src/main/resources/db/migration/postgresql/create.sql)

## Resolución
1. URL de la aplicación
http://3.15.181.222/swagger-ui.html
2. Código fuente
[GIT](https://gitlab.com/xesar.miguel/security)
3. Link del Postman (https://www.postman.com/collections/d544d0936f1226aad6db)

   3.1 Importar la colección desde la opción import del PostMan mediante el link

![postman](https://gitlab.com/xesar.miguel/security/-/raw/master/security-boot/src/main/resources/db/migration/postgresql/postman.png)

