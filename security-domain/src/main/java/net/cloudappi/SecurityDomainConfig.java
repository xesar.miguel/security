package net.cloudappi;

import org.springframework.context.annotation.*;

import net.cloudappi.security.model.port.*;
import net.cloudappi.security.model.service.*;

@Configuration
public class SecurityDomainConfig {

	@Bean
	public OrquestadorServiceImpl orquestadorServiceImpl(UserPort userPort, AddressPort addressPort) {
		return new OrquestadorServiceImpl(userPort, addressPort);
	}
}
