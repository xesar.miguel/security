package net.cloudappi.security.model.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import net.cloudappi.security.model.entity.Address;
import net.cloudappi.security.model.entity.User;
import net.cloudappi.security.model.port.AddressPort;
import net.cloudappi.security.model.port.UserPort;
import net.cloudappi.security.model.vo.AddressUpdate;
import net.cloudappi.security.model.vo.UserAddress;
import net.cloudappi.security.model.vo.UserUpdate;

@Transactional
public class OrquestadorServiceImpl implements OrquestadorService {

	private final UserPort userPort;
	private final AddressPort addressPort;

	public OrquestadorServiceImpl(UserPort userPort, AddressPort addressPort) {
		this.userPort = userPort;
		this.addressPort = addressPort;
	}
	
	@Override
	public List<UserAddress> findUsers() {
		List<UserAddress> listResponse = new ArrayList<UserAddress>();
		List<User> listUser = userPort.findUsers();
		for (User user : listUser) {
			listResponse.add(new UserAddress(user, addressPort.findAddress(user.getUserId())));
		}
		return listResponse;
	}

	@Override
	public UserAddress createUser(User userToCreate, Address addressToCreate) {
		UserAddress userAddress = new UserAddress();
		User user = userPort.createUser(userToCreate);
		addressToCreate.setUserId(user.getUserId());
		Address address = addressPort.createAddress(addressToCreate);
		userAddress.setUser(user);
		userAddress.setAddress(address);
		return userAddress;		
	}

	@Override
	public UserAddress findUser(Long userId) {	
		UserAddress response = null;
		User user = userPort.findUser(userId);
		if (user != null) {
			response =	new UserAddress(user, addressPort.findAddress(user.getUserId()));	
		}
		return response;
	}

	@Override
	public boolean existsUser(Long userId) {
		boolean exists = userPort.existsUser(userId);
	    return exists;
	}

	@Override
	public long updateUser(UserUpdate userUpdate, AddressUpdate addressUpdate) {
		long updateCount = userPort.updateUser(userUpdate);
		if (addressPort.findAddress(userUpdate.getUserId()) != null) {
			addressPort.updateAddress(addressUpdate);
		}		
	    return updateCount;	    		
	}

	@Override
	public void deleteUser(Long userId) {		
		addressPort.deleteAddress(userId);
		if (this.existsUser(userId))
			userPort.deleteUser(userId);
	}

}
