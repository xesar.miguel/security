package net.cloudappi.security.model.vo;

import java.time.OffsetDateTime;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.escalableapps.framework.core.validation.constraints.DatetimeRange;

import lombok.NoArgsConstructor;
import lombok.ToString;
import net.cloudappi.security.model.SqlConstants;

/**
 * Se registra todos los usuarios<br>
 * <strong>t_user</strong>
 */
@NoArgsConstructor
@ToString
public class UserUpdate {

	public UserUpdate(Long userId) {
		this.userId = userId;
	}

	/**
	 * Nombre del usuario<br>
	 * <strong>user_vname character varying</strong>
	 */
	@Size(min = 1, max = 100)
	private String name;

	/**
	 * Correo electronico del usuario<br>
	 * <strong>user_vemail character varying</strong>
	 */
	@Size(min = 1, max = 200)
	private String email;

	/**
	 * Fecha de cumpleaños del usuario<br>
	 * <strong>user_dbirthdate timestamp without time zone</strong>
	 */
	@DatetimeRange(min = SqlConstants.Limits.TIMESTAMP_MIN, max = SqlConstants.Limits.TIMESTAMP_MAX)
	private OffsetDateTime birthdate;

	@NotNull
	@Min(1)
	private Long userId;

	/**
	 * Nombre del usuario<br>
	 * <strong>user_vname character varying</strong>
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Nombre del usuario<br>
	 * <strong>user_vname character varying</strong>
	 */
	public String getName() {
		return name;
	}

	/**
	 * Correo electronico del usuario<br>
	 * <strong>user_vemail character varying</strong>
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Correo electronico del usuario<br>
	 * <strong>user_vemail character varying</strong>
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Fecha de cumpleaños del usuario<br>
	 * <strong>user_dbirthdate timestamp without time zone</strong>
	 */
	public void setBirthdate(OffsetDateTime birthdate) {
		this.birthdate = birthdate;

	}

	/**
	 * Fecha de cumpleaños del usuario<br>
	 * <strong>user_dbirthdate timestamp without time zone</strong>
	 */
	public OffsetDateTime getBirthdate() {
		return birthdate;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}