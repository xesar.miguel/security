package net.cloudappi.security.model.vo;

import net.cloudappi.security.model.entity.Address;
import net.cloudappi.security.model.entity.User;

public class UserAddress {

	private User user;
	private Address address;

	public UserAddress() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserAddress(User user, Address address) {
		super();
		this.user = user;
		this.address = address;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

}
