package net.cloudappi.security.model.vo;

public class UserAddressUpdate {

	private UserUpdate user;
	private AddressUpdate address;

	public UserAddressUpdate() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserAddressUpdate(UserUpdate user, AddressUpdate address) {
		super();
		this.user = user;
		this.address = address;
	}

	public UserUpdate getUser() {
		return user;
	}

	public void setUser(UserUpdate user) {
		this.user = user;
	}

	public AddressUpdate getAddress() {
		return address;
	}

	public void setAddress(AddressUpdate address) {
		this.address = address;
	}

}
