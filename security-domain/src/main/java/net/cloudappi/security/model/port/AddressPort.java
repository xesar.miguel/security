package net.cloudappi.security.model.port;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import net.cloudappi.security.model.entity.Address;
import net.cloudappi.security.model.vo.AddressUpdate;

public interface AddressPort {

	Address createAddress( //
			@NotNull @Valid Address addressToCreate //
	);

	Address findAddress( //
			@NotNull @Min(1) Long userId //
	);

	long updateAddress( //
			@NotNull @Valid AddressUpdate addressUpdate //
	);

	void deleteAddress( //
			@NotNull @Min(1) Long userId //
	);
}