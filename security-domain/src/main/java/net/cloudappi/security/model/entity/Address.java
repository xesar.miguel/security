package net.cloudappi.security.model.entity;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Se registra todos los usuarios<br>
 * <strong>t_addres</strong>
 */
@NoArgsConstructor
@ToString
public class Address {

	/**
	 * ID AUTOGENERADO<br>
	 * <strong>addr_iident bigint NOT NULL</strong>
	 */
	@Min(1)
	private Long addressId;

	/**
	 * Nombre del usuario<br>
	 * <strong>user_iident bigint</strong>
	 */
	@Min(1)
	private Long userId;

	/**
	 * Nombre de la calle<br>
	 * <strong>addr_vstreet character varying</strong>
	 */
	@Size(min = 1, max = 100)
	private String street;

	/**
	 * Nombre del estado<br>
	 * <strong>addr_vstate character varying</strong>
	 */
	@Size(min = 1, max = 100)
	private String state;

	/**
	 * Nombre de la ciudad<br>
	 * <strong>addr_vcity character varying</strong>
	 */
	@Size(min = 1, max = 100)
	private String city;

	/**
	 * Nombre del país<br>
	 * <strong>addr_vcountry character varying</strong>
	 */
	@Size(min = 1, max = 100)
	private String country;

	/**
	 * Código Postal<br>
	 * <strong>addr_vzip character varying</strong>
	 */
	@Size(min = 1, max = 100)
	private String zip;

	public Address(Long addressId) {
		this.addressId = addressId;
	}

	/**
	 * ID AUTOGENERADO<br>
	 * <strong>addr_iident bigint NOT NULL</strong>
	 */
	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}

	/**
	 * ID AUTOGENERADO<br>
	 * <strong>addr_iident bigint NOT NULL</strong>
	 */
	public Long getAddressId() {
		return addressId;
	}

	/**
	 * Nombre del usuario<br>
	 * <strong>user_iident bigint</strong>
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * Nombre del usuario<br>
	 * <strong>user_iident bigint</strong>
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * Nombre de la calle<br>
	 * <strong>addr_vstreet character varying</strong>
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * Nombre de la calle<br>
	 * <strong>addr_vstreet character varying</strong>
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * Nombre del estado<br>
	 * <strong>addr_vstate character varying</strong>
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Nombre del estado<br>
	 * <strong>addr_vstate character varying</strong>
	 */
	public String getState() {
		return state;
	}

	/**
	 * Nombre de la ciudad<br>
	 * <strong>addr_vcity character varying</strong>
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Nombre de la ciudad<br>
	 * <strong>addr_vcity character varying</strong>
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Nombre del país<br>
	 * <strong>addr_vcountry character varying</strong>
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Nombre del país<br>
	 * <strong>addr_vcountry character varying</strong>
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Código Postal<br>
	 * <strong>addr_vzip character varying</strong>
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * Código Postal<br>
	 * <strong>addr_vzip character varying</strong>
	 */
	public String getZip() {
		return zip;
	}
}