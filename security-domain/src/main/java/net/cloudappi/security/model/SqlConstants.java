package net.cloudappi.security.model;

import com.escalableapps.framework.core.model.db.Postgres;

public interface SqlConstants {

  public static interface Types {
    public final static String CHAR = Postgres.Types.CHAR;
    public final static String VARCHAR = Postgres.Types.VARCHAR;
    public final static String CLOB = Postgres.Types.CLOB;
    public final static String SMALLINT = Postgres.Types.SMALLINT;
    public final static String INTEGER = Postgres.Types.INTEGER;
    public final static String BIGINT = Postgres.Types.BIGINT;
    public final static String REAL = Postgres.Types.REAL;
    public final static String DOUBLE = Postgres.Types.DOUBLE;
    public final static String DECIMAL = Postgres.Types.DECIMAL;
    public final static String BOOLEAN = Postgres.Types.BOOLEAN;
    public final static String DATE = Postgres.Types.DATE;
    public final static String TIMESTAMP = Postgres.Types.TIMESTAMP;
  }

  public static interface Limits {
    public final static String DATE_MIN = Postgres.Limits.DATE_MIN;
    public final static String DATE_MAX = Postgres.Limits.DATE_MAX;

    public final static String TIMESTAMP_MIN = Postgres.Limits.TIMESTAMP_MIN;
    public final static String TIMESTAMP_MAX = Postgres.Limits.TIMESTAMP_MAX;
  }
}