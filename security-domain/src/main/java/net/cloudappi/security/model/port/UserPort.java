package net.cloudappi.security.model.port;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import net.cloudappi.security.model.entity.User;
import net.cloudappi.security.model.vo.UserUpdate;

public interface UserPort {

	User createUser( //
			@NotNull @Valid User userToCreate //
	);

	User findUser( //
			@NotNull @Min(1) Long userId //
	);

	List<@NotNull @Valid User> findUsers();

	boolean existsUser( //
			@NotNull @Min(1) Long userId //
	);

	long updateUser( //
			@NotNull @Valid UserUpdate userUpdate //
	);

	void deleteUser( //
			@NotNull @Min(1) Long userId //
	);
}