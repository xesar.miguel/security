package net.cloudappi.security.model.service;

import java.util.List;

import net.cloudappi.security.model.entity.Address;
import net.cloudappi.security.model.entity.User;
import net.cloudappi.security.model.vo.AddressUpdate;
import net.cloudappi.security.model.vo.UserAddress;
import net.cloudappi.security.model.vo.UserUpdate;

public interface OrquestadorService {

	List<UserAddress> findUsers();

	UserAddress createUser(User user, Address address);

	UserAddress findUser(Long userId);

	long updateUser(UserUpdate userUpdate, AddressUpdate addressUpdate);

	void deleteUser(Long userId);

	boolean existsUser(Long userId);

}
