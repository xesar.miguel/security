package net.cloudappi.security.model.vo;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Se registra todos los usuarios<br>
 * <strong>t_addres</strong>
 */
@NoArgsConstructor
@ToString
public class AddressUpdate {

	/**
	 * Nombre del usuario<br>
	 * <strong>user_iident bigint</strong>
	 */
	@Min(1)
	private Long userId;

	/**
	 * Nombre de la calle<br>
	 * <strong>addr_vstreet character varying</strong>
	 */
	@Size(min = 1, max = 100)
	private String street;

	/**
	 * Nombre del estado<br>
	 * <strong>addr_vstate character varying</strong>
	 */
	@Size(min = 1, max = 100)
	private String state;

	/**
	 * Nombre de la ciudad<br>
	 * <strong>addr_vcity character varying</strong>
	 */
	@Size(min = 1, max = 100)
	private String city;

	/**
	 * Nombre del país<br>
	 * <strong>addr_vcountry character varying</strong>
	 */
	@Size(min = 1, max = 100)
	private String country;

	/**
	 * Código Postal<br>
	 * <strong>addr_vzip character varying</strong>
	 */
	@Size(min = 1, max = 100)
	private String zip;

	/**
	 * Nombre del usuario<br>
	 * <strong>user_iident bigint</strong>
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * Nombre del usuario<br>
	 * <strong>user_iident bigint</strong>
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * Nombre de la calle<br>
	 * <strong>addr_vstreet character varying</strong>
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * Nombre de la calle<br>
	 * <strong>addr_vstreet character varying</strong>
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * Nombre del estado<br>
	 * <strong>addr_vstate character varying</strong>
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Nombre del estado<br>
	 * <strong>addr_vstate character varying</strong>
	 */
	public String getState() {
		return state;
	}

	/**
	 * Nombre de la ciudad<br>
	 * <strong>addr_vcity character varying</strong>
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Nombre de la ciudad<br>
	 * <strong>addr_vcity character varying</strong>
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Nombre del país<br>
	 * <strong>addr_vcountry character varying</strong>
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Nombre del país<br>
	 * <strong>addr_vcountry character varying</strong>
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Código Postal<br>
	 * <strong>addr_vzip character varying</strong>
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * Código Postal<br>
	 * <strong>addr_vzip character varying</strong>
	 */
	public String getZip() {
		return zip;
	}

}